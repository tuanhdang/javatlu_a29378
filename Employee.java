package Employee;

public class Employee {
	private String FirstName;
	private String LastName;
	private double MonthlySalary;
	
	public String getFirstName(){
		return FirstName;
	}
	public void setFirstName(String FirstName){
		this.FirstName = FirstName;
	}
	
	public String getLastName(){
		return LastName;
	}
	public void setLastName(String LastName){
		this.LastName = LastName;
	}
	
	public double getMonthlySalary(){
		return MonthlySalary;
	}
	public void setMonthlySalary(double MonthlySalary){
		if (MonthlySalary < 0)
			this.MonthlySalary = 0.0;
		else
			this.MonthlySalary = MonthlySalary;
	}
	
	public void DisplayYearlySalary(){
		System.out.println("YearlySalary: " + MonthlySalary*12);
	}
}
