package Employee;

public class EmployeeTest {
	public static void main(String[] args) {
		Employee One = new Employee();
		Employee Two = new Employee();
		
		One.setMonthlySalary(-500);
		System.out.println(One.getMonthlySalary());

		
		One.setMonthlySalary(1000);
		Two.setMonthlySalary(1000);
		
		One.DisplayYearlySalary();
		Two.DisplayYearlySalary();
		
		One.setMonthlySalary(1000*110/100);
		
		One.DisplayYearlySalary();
		Two.DisplayYearlySalary();
	}
}
